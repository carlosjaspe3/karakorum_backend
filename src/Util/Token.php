<?php

namespace App\Util;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

class Token
{
    public function generate()
    {
        return substr(md5(random_bytes(15)), -15);
    }
}
