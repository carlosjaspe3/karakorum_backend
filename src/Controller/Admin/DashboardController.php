<?php

namespace App\Controller\Admin;

use App\Entity\Contact1;
use App\Entity\Contact;
use App\Entity\Document;
use App\Entity\PerformanceData;
use App\Entity\PerformanceDataUser;
use App\Entity\ReferralConfig;
use App\Entity\Transaction;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\Admin\TransactionCrudController;
use App\Controller\Admin\TransactionPerforCrudController;
use App\Controller\Admin\UserAdminCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use Doctrine\ORM\EntityManagerInterface;

class DashboardController extends AbstractDashboardController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/admin", name = "dashboard_admin")
     */
    public function index(): Response
    {

        $em = $this->getDoctrine()->getManager();

        // $sql = 'SELECT count(*) as total FROM user';
        // $statement = $em->getConnection()->prepare($sql);
        // $statement->execute();
        // $return ['usersAll'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT count(*) as total FROM user WHERE enabled=1 and (is_deleted IS NULL OR is_deleted = 0)';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['usersAct'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT count(*) as total FROM user WHERE enabled=1 and deposited > 0 and (is_deleted IS NULL OR is_deleted = 0)';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['usersActInv'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT count(*) as total FROM user WHERE created_at >= NOW() - INTERVAL 1 DAY; ';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['users24'] = $statement->fetchAll()[0]['total'];


        $sql = 'SELECT sum(deposited) as total FROM user; ';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['totalD'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT sum(balance) as total FROM user; ';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['totalB'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT sum(amount) as total FROM transaction WHERE type="withdrawal" AND status = "success"; ';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['totalW'] = $statement->fetchAll()[0]['total'];

        $return['totalDW'] = $return ['totalD'] - $return ['totalW'];


        $sql = 'SELECT sum(balance) as total FROM user WHERE balance >=50; ';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return['totalDispW'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT IF(sum(amount) IS NULL,0, sum(amount)) as total FROM transaction WHERE type="deposit" AND whitdrawal IS NULL AND status = "success" AND DATEDIFF(NOW(), transaction.date) >= 180';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return['totalDepoL'] = $statement->fetchAll()[0]['total'];
        

        return $this->render('admin/my-dashboard.html.twig', $return);
        
        /* $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(UserCrudController::class)->generateUrl()); */
    }


    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<strong>Karakorum</strong>');
    }

    public function configureCrud(): Crud
    {
        return Crud::new()
            ->setDateFormat('dd/MM/yyyy')
            ->setDateTimeFormat('dd/MM/yyyy HH:mm:ss')
            ->setTimeFormat('HH:mm')
            ->showEntityActionsAsDropdown()
        ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::section('Principal');
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        
        yield MenuItem::section('Usuarios');
        yield MenuItem::linkToCrud('Clientes', 'fas fa-hiking', User::class);

        yield MenuItem::section('Transacciones');
        yield MenuItem::linkToCrud('Depósitos y Retiros', 'fas fa-credit-card', Transaction::class)->setController(TransactionCrudController::class);
        yield MenuItem::linkToCrud('Rendimientos', 'fas fa-receipt', Transaction::class)->setController(TransactionPerforCrudController::class);
        yield MenuItem::linkToCrud('Eliminados', 'fas fa-dumpster-fire', Transaction::class)->setController(TransactionDeletedCrudController::class);

        yield MenuItem::section('Configuraciones');
        yield MenuItem::linkToCrud('Rendimientos', 'fas fa-sliders-h', PerformanceData::class);
        yield MenuItem::linkToCrud('Rend. Usuario', 'fas fa-sliders-h', PerformanceDataUser::class);
        yield MenuItem::linkToCrud('Referidos', 'fas fa-sliders-h', ReferralConfig::class);

        yield MenuItem::section('Otros');
        yield MenuItem::linkToCrud('Soporte', 'fas fa-headset', Contact::class);
        yield MenuItem::linkToCrud('Contacto', 'fas fa-address-book', Contact1::class);
        yield MenuItem::linkToCrud('Documentos', 'fas fa-file-contract', Document::class);

        yield MenuItem::section('Admin');
        yield MenuItem::linkToCrud('Administradores', 'fas fa-user-shield', User::class)
            ->setController(UserAdminCrudController::class);
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addCssFile('css/admin.css')
            ->addCssFile('css/icons.min.css')
            ->addCssFile('css/app1.min.css')
        ;
    }
}
