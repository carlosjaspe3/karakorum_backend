<?php

namespace App\Controller\Admin;

use App\Entity\PerformanceData;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class PerformanceDataCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PerformanceData::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Configuración de Rendimiento')
            ->setEntityLabelInPlural('Configuración de Rendimientos')
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable('delete', 'new')
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel('Detalle');
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-pencil')->setLabel('Modificar');
            })
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $percentageMin = NumberField::new('percentageMin', 'Min (%)');
        $percentageMax = NumberField::new('percentageMax', 'Max (%)');
        $percentageExchangeLimit = NumberField::new('percentageExchangeLimit', 'Porcentaje límite de cambio (%)');
        $minimunAmountToReinvest = NumberField::new('minimunAmountToReinvest', 'Min monto reinversión');
        $minAmountDeposit = NumberField::new('minAmountDeposit', 'Min monto depósito');
        $maxAmountDeposit = NumberField::new('maxAmountDeposit', 'Max monto depósito');
        $daysDelayPerformance = IntegerField::new('daysDelayPerformance', 'Dias para rendimiento');
        $minAmountWhitdrawal = NumberField::new('minAmountWhitdrawal', 'Min monto retiro');
        $daysDelayWhitdrawal = IntegerField::new('daysDelayWhitdrawal', 'Dias para retiro');
        $createdAt = DateTimeField::new('createdAt', 'Fecha de creación');
        $updatedAt = DateTimeField::new('updatedAt', 'Fecha de actualización');
        $performanceDataUser = AssociationField::new('performanceDataUser', 'Datos Rendimiento Usuario');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$percentageMin, $percentageMax, $percentageExchangeLimit, $minimunAmountToReinvest];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$percentageMin, $percentageMax, $percentageExchangeLimit, $minimunAmountToReinvest, $minAmountDeposit, $maxAmountDeposit, $daysDelayPerformance, $minAmountWhitdrawal, $daysDelayWhitdrawal, $performanceDataUser, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$percentageMin, $percentageMax, $percentageExchangeLimit, $minimunAmountToReinvest, $minAmountDeposit, $maxAmountDeposit, $daysDelayPerformance, $minAmountWhitdrawal, $daysDelayWhitdrawal, $performanceDataUser, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$percentageMin, $percentageMax, $percentageExchangeLimit, $minimunAmountToReinvest, $minAmountDeposit, $maxAmountDeposit, $daysDelayPerformance, $minAmountWhitdrawal, $daysDelayWhitdrawal, $performanceDataUser, $createdAt, $updatedAt];
        }
    }
}
