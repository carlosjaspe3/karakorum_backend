<?php

namespace App\DataFixtures;

use App\Entity\PerformanceData;
use App\Entity\PerformanceDataUser;
use App\Entity\ReferralConfig;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ReferralFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $iterator = 1;
        while ($iterator <= 10) {
            $referralConfig = (new ReferralConfig())
                ->setLevel($iterator)
                ->setMinAmountDeposited($iterator * 500)
                ->setMinAmountDepositedReferrals($iterator * 800)
                ->setPercentage($iterator / 2);
            $manager->persist($referralConfig);
            $iterator += 1;
        }
        $manager->flush();
    }
}
