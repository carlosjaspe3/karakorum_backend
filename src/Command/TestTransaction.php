<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use App\Entity\Transaction;
use App\Entity\User;
use App\Entity\Referral;
use App\Util\Mailer;
use DateInterval;
use DateTime;


class TestTransaction extends Command
{
    private $em;
    private $mailer;
 
    public function __construct(EntityManagerInterface $em, LoggerInterface $loggerInterface, Mailer $mailer)
    {
        parent::__construct();
        $this->em = $em;
        $this->logger = $loggerInterface;
        $this->mailer = $mailer;
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:test-transaction';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        
        // pending: { title: "Pendiente" },
        // confirming: {title: "Confirmando"},
        // success: { title: "Aprobado" },
        // expired: { title: "Expirado" },
        // incomplete: {title: "Incompleto"}

        //Cargo todas las transacciones existentes del tipo deposit y distintas a success

        $transactions = $this->em
                ->createQueryBuilder()
                ->select('t')
                ->from(Transaction::class, 't')
                ->where('t.type = :type')
                ->andWhere('t.id = :id')
                ->setParameter('type', 'deposit')
                ->setParameter('id', 20723)
                ->getQuery()
                ->getResult();
        
        
        foreach($transactions as $t){
            $output->writeln('ID: '.$t->getId());
            $output->writeln('USER: '.$t->getUser());

            $msg = [];
            $msg['txs'] = "txstxstxstxstxstxstxstxstxs";
            $msg['remaining'] = '0.00001000';

            $t->setMessage($msg);
            $this->em->persist($t);
            $this->em->flush();

            
        }     
        

        return Command::SUCCESS;
    }

   
}
