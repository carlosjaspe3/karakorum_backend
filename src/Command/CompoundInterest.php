<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\PerformanceData;
use App\Entity\Transaction;
use App\Entity\User;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Constraints\Date;


class CompoundInterest extends Command
{
    private $em;
    private $usr;
    public function __construct(EntityManagerInterface $em, LoggerInterface $loggerInterface)
    {
        parent::__construct();
        $this->em = $em;
        $this->logger = $loggerInterface;
    }
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:compound-interest';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting');
        $output->writeln('Starting the CompoundInterest');

        $performanceData = $this->em->getRepository(PerformanceData::class)->findOneBy([]);
        if (!$performanceData) {
            $output->writeln('Configure performance data');
            return Command::FAILURE;
        };        
        $minAmountToReinvest = $performanceData->getMinimunAmountToReinvest();
        /* CODIGO DE REINVERSION */

        $users = $this
            ->em
            ->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.enabled = 1')
            ->andWhere('u.balance >= :minAmountToReinvest')
            ->andWhere('u.compoundInterest = 1')
            //->where('u.enabled = 1', 'u.balance >= :minAmountToReinvest', 'u.compoundInterest = 1', 'u.deletedAt = :deletedAt')
            ->setParameter('minAmountToReinvest', $minAmountToReinvest)       
            //->setParameter('deletedAt', NULL)  
            ->getQuery()
            ->getResult();
        #var_dump($users->getSQL());
        #exit;
        if (!$users) {
            $output->writeln('Not users meeting the condition');
            return Command::FAILURE;
        };
        
        foreach ($users as $user) {

            if ($user->getCompoundInterest() == 1 && 
                $user->getBalance() >= $performanceData->getMinimunAmountToReinvest()) {
                $output->writeln('User -> '. $user->getEmail().' Reinveritr -> '.$performanceData->getMinimunAmountToReinvest().' Balance -> '.$user->getBalance());
                
                // Restamos del balance con una Buy Transaction
                $buyTransaction = (new Transaction())
                    ->setType(Transaction::TYPE_BUY)
                    ->setUser($user)
                    ->setAmount($performanceData->getMinimunAmountToReinvest())
                    ->setTransaction(NULL)
                    ->setDate(new DateTime())
                    ->setStatus("success")
                    ->setMessage(['withdrawal buy'])
                    ->setChargeUUID('');
                $this->em->persist($buyTransaction);

                // Realizamos un deposito del AmountToReinvest
                $reinvestTransaction = (new Transaction())
                    ->setType(Transaction::TYPE_DEPOSIT)
                    ->setUser($user)
                    ->setAmount($performanceData->getMinimunAmountToReinvest())
                    ->setTransaction($buyTransaction)
                    ->setDate(new DateTime())
                    ->setStatus("success")
                    ->setMessage(['Buy with Balance'])
                    ->setChargeUUID('');

                $this->em->persist($reinvestTransaction);
                $this->em->flush();

                // Actualizamos el balance
                $Deposited = $this->em->getRepository(User::class)->getDeposited($user->getId());
                #var_dump($Deposited);
                $user->setDeposited($Deposited);

                $Balance = $this->em->getRepository(User::class)->getBalance($user->getId());
                #var_dump($Balance);
                $user->setBalance($Balance);

                #$user->setBalance();
                #$user->setDeposited();
                $this->em->flush();
                
                $output->writeln('reinvest');

            }

        }

        $output->writeln('Finished');
        $output->writeln('Finished CompoundInterest task');
        return Command::SUCCESS;

    }
}

