<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface, UserLoaderInterface
{
    private $context;

    public function __construct(ManagerRegistry $registry, RequestStack $requestStack)
    {
        parent::__construct($registry, User::class);
        
        if($requestStack->getCurrentRequest()){
            $this->context = explode("/", $requestStack->getCurrentRequest()->getPathInfo())[1];
        }
        
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function loadUserByUsername($email)
    {
        $qb = $this
            ->createQueryBuilder('u')
            ->select('u')
            ->where('u.email = :email')
            ->andWhere('u.enabled = :enabled')
            ->setParameter('email', $email)
            ->setParameter('enabled',true);

        if ($this->context == 'admin') {
            return $qb
                ->andWhere('u.roles LIKE :role')
                ->setParameter('role', '%ROLE_ADMIN%')
                ->getQuery()
                ->getOneOrNullResult();
            
        }
        if ($this->context == 'api') {
            return $qb
                ->getQuery()
                ->getOneOrNullResult();
            
        }
    }
    
    public function getBalance($user_id){
        
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(t.amount) as total FROM transaction t WHERE t.status = "success" AND t.type IN ("referral", "performance") AND t.deletedAt IS NULL AND t.user_id = "'.$user_id.'"';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        
        $entrada = $stmt->fetchAll()[0]['total'];

        $sql = 'SELECT SUM(t.amount) as total FROM transaction t WHERE t.status = "success" AND t.type IN ("withdrawal", "buy") AND t.whitdrawal IS NULL AND t.deletedAt IS NULL AND t.user_id = "'.$user_id.'"';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        
        $salida = $stmt->fetchAll()[0]['total'];
        
        $total = $entrada-$salida;
        return round($total,5);
    }

    public function getReferred($user_id){
        
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(t.amount) as total FROM transaction t WHERE t.status = "success" AND t.type IN ("referral") AND t.deletedAt IS NULL AND t.user_id = "'.$user_id.'"';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        
        $total = $stmt->fetchAll()[0]['total'];

        return round($total,5);
    }

    public function getPerformance($user_id){
        
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(t.amount) as total FROM transaction t WHERE t.status = "success" AND t.type IN ("performance") AND t.deletedAt IS NULL AND t.user_id = "'.$user_id.'"';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        
        $total = $stmt->fetchAll()[0]['total'];

        return round($total,5);
    }

    public function getDeposited($user_id){
        
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(t.amount) as total FROM transaction t WHERE t.status = "success" AND t.type IN ("deposit") AND t.whitdrawal IS NULL AND t.deletedAt IS NULL AND t.user_id = "'.$user_id.'"';
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        
        $total = $stmt->fetchAll()[0]['total'];

        return round($total,5);
    }


    public function getAllUsersClient()
    {   
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT u.id, u.email FROM user u WHERE u.roles NOT LIKE  "%admin%" AND u.enabled=1 AND u.deletedAt IS NULL ORDER BY u.id DESC';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getUsersDetail($id)
    {   
        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT u.* FROM user u WHERE u.id = "'.$id.'"';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
    
}
