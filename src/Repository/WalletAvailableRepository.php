<?php

namespace App\Repository;

use App\Entity\WalletAvailable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method WalletAvailable|null find($id, $lockMode = null, $lockVersion = null)
 * @method WalletAvailable|null findOneBy(array $criteria, array $orderBy = null)
 * @method WalletAvailable[]    findAll()
 * @method WalletAvailable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WalletAvailableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WalletAvailable::class);
    }

    // /**
    //  * @return WalletAvailable[] Returns an array of WalletAvailable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WalletAvailable
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
