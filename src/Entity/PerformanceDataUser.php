<?php

namespace App\Entity;

use App\Repository\PerformanceDataUserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PerformanceDataUserRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class PerformanceDataUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=2)
     */
    private $minAmount;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=2)
     */
    private $maxAmount;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $percentage;

    /**
     * @ORM\ManyToOne(targetEntity=PerformanceData::class, inversedBy="performanceDataUser")
     * @ORM\JoinColumn(nullable=false)
     */
    private $performanceData;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMinAmount(): ?string
    {
        return $this->minAmount;
    }

    public function setMinAmount(string $minAmount): self
    {
        $this->minAmount = $minAmount;

        return $this;
    }

    public function getMaxAmount(): ?string
    {
        return $this->maxAmount;
    }

    public function setMaxAmount(string $maxAmount): self
    {
        $this->maxAmount = $maxAmount;

        return $this;
    }

    public function getPercentage(): ?string
    {
        return $this->percentage;
    }

    public function setPercentage(string $percentage): self
    {
        $this->percentage = $percentage;
        return $this;
    }

    public function getPerformanceData(): ?PerformanceData
    {
        return $this->performanceData;
    }

    public function setPerformanceData(?PerformanceData $performanceData): self
    {
        $this->performanceData = $performanceData;

        return $this;
    }
    public function getPercentageCal()
    {
        return $this->getPercentage()/100;
    }

    public function __toString()
    {
        return $this->minAmount.' - '.$this->maxAmount.' --> '.$this->percentage.'%';
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}
