<?php

namespace App\Entity;

use App\Repository\ReferralConfigRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity
 * @ApiResource(
 *     itemOperations={
 *         "get",
 *     },
 *     collectionOperations={
 *         "get",
 *     },
 * )
 * @ORM\HasLifecycleCallbacks
 */
class ReferralConfig
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     */
    private $minAmountDeposited;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     */
    private $minAmountDepositedReferrals;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=7)
     */
    private $percentage;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMinAmountDeposited(): ?string
    {
        return $this->minAmountDeposited;
    }

    public function setMinAmountDeposited(?string $minAmountDeposited): self
    {
        $this->minAmountDeposited = $minAmountDeposited;

        return $this;
    }

    public function getMinAmountDepositedReferrals(): ?string
    {
        return $this->minAmountDepositedReferrals;
    }

    public function setMinAmountDepositedReferrals(?string $minAmountDepositedReferrals): self
    {
        $this->minAmountDepositedReferrals = $minAmountDepositedReferrals;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getPercentage(): ?string
    {
        return $this->percentage;
    }

    public function setPercentage(string $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getPercentageToCalculate()
    {
        return $this->percentage/100;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}
