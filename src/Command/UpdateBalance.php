<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Doctrine\Common\Collections\Criteria;

class UpdateBalance extends Command
{
    protected static $defaultName = 'app:update-balance';

    private $em;
 
    public function __construct(EntityManagerInterface $em, LoggerInterface $loggerInterface)
    {
        parent::__construct();
        $this->em = $em;
        $this->logger = $loggerInterface;
    }

    
    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Starting: '.date('Y-m-d H:i:s'));
        
        $users = $this->em->getRepository(User::class)->getAllUsersClient();
        
        // $users = $this->em
        //     ->createQueryBuilder()
        //     ->select('u')
        //     ->from(User::class, 'u')
        //     ->getQuery()
        //     ->getResult();

        $output->writeln('Load: '.date('Y-m-d H:i:s'));
        
        
        foreach($users as $u){
            
            $usr = $this->em->find(User::class, $u['id']);
            
            
            #if($usr->getId() == 2){
                $output->writeln('Email: '.$usr->getEmail());

                $output->writeln('Referred: '.date('Y-m-d H:i:s'));
                $Referred = $this->em->getRepository(User::class)->getReferred($usr->getId());
                #var_dump($Referred);
                $usr->setReferred($Referred);
                
                $output->writeln('Performance: '.date('Y-m-d H:i:s'));
                $Performance = $this->em->getRepository(User::class)->getPerformance($usr->getId());
                #var_dump($Performance);
                $usr->setPerformance($Performance);
                
                $output->writeln('Balance: '.date('Y-m-d H:i:s'));
                $Balance = $this->em->getRepository(User::class)->getBalance($usr->getId());
                #var_dump($Balance);
                $usr->setBalance($Balance);

                $output->writeln('Deposited: '.date('Y-m-d H:i:s'));
                $Deposited = $this->em->getRepository(User::class)->getDeposited($usr->getId());
                #var_dump($Deposited);
                $usr->setDeposited($Deposited);
                
                $this->em->flush();
            #}
	}

        $output->writeln('Finish: '.date('Y-m-d H:i:s'));

        return Command::SUCCESS;
    }
}

