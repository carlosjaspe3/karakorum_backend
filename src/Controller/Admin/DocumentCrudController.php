<?php

namespace App\Controller\Admin;

use App\Entity\Document;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class DocumentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Document::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Documento')
            ->setEntityLabelInPlural('Documentos')
        ;
    }

    public function configureActions(Actions $actions): Actions
    {

        return $actions

            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel('Detalle');
            })

            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-pencil')->setLabel('Modificar');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setIcon('fa fa-trash')->setLabel('Eliminar');
            })
        ;
        
    }

    public function configureFields(string $pageName): iterable
    {
        //$name = TextField::new('name', 'Título');
        $doc = TextField::new('doc', 'Documento')   ;
        $docFile = Field::new('docFile', 'Archivo')->setFormType(VichFileType::class)->onlyOnForms();
        $description = TextareaField::new('description', 'Descripción');
        $createdAt = DateTimeField::new('createdAt', 'Fecha de creación');
        $updatedAt = DateTimeField::new('updatedAt', 'Fecha de actualización');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$doc, $description, $createdAt];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$doc, $docFile, $description, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$doc, $docFile, $description, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$doc, $docFile, $description, $createdAt, $updatedAt];
        }
    }

}
