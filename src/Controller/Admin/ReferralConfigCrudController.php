<?php

namespace App\Controller\Admin;

use App\Entity\ReferralConfig;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class ReferralConfigCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ReferralConfig::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Configuración de Réferido')
            ->setEntityLabelInPlural('Configuración de Réferidos')
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel('Detalle');
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-pencil')->setLabel('Modificar');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setIcon('fa fa-trash')->setLabel('Eliminar');
            })
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $minAmountDeposited = NumberField::new('minAmountDeposited', 'Monto Min Depósitos');
        $minAmountDepositedReferrals = NumberField::new('minAmountDepositedReferrals', 'Monto Min Depósitos Por Referidos');
        $level = IntegerField::new('level', 'Nivel');
        $percentage = NumberField::new('percentage', 'Porcentaje (%)');
        $createdAt = DateTimeField::new('createdAt', 'Fecha de creación');
        $updatedAt = DateTimeField::new('updatedAt', 'Fecha de actualización');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$level, $percentage, $minAmountDeposited, $minAmountDepositedReferrals];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $minAmountDeposited, $minAmountDepositedReferrals, $level, $percentage, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$minAmountDeposited, $minAmountDepositedReferrals, $level, $percentage, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$level, $percentage, $minAmountDeposited, $minAmountDepositedReferrals];
        }
    }
}
