<?php

namespace App\Controller\Api;

use App\Entity\BasePerformance;
use App\Entity\PerformanceData;
use App\Entity\Transaction;
use App\Entity\User;
use App\Exception\ApiBadRequestException;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use App\Util\Mailer;

class WhitdrawalController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $entityManagerInterface) {
        $this->em = $entityManagerInterface;
    }

    /**
     * @Route("/api/whitdrawal", name="api_whitdrawal", methods="POST")
     */
    public function whitdrawal(Request $request, SerializerInterface $serializerInterface, Mailer $mailer)
    {
        $content = \json_decode($request->getContent(), true);
        $context = $content["context"];
        $dataBaseApp = $this->em->getRepository(PerformanceData::class)->findOneBy([]);
        $user = $this->getUser();
        $now = new DateTime();

        $g = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
        
        $conn = $this->em->getConnection();
        $sql = 'SELECT t.id, t.type, t.amount, t.created_at, t.status '
                .'FROM transaction AS t '
                .'WHERE t.type = "'.Transaction::TYPE_WITHDRAWAL.'" '
                .'AND t.user_id = '.$user->getId().' '
                .'ORDER BY t.created_at DESC LIMIT 1';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $rt_transaction = $stmt->fetchAll();


        $conn = $this->em->getConnection();
        $sql = 'SELECT t.id, t.type, t.amount, t.created_at, t.status, t.whitdrawal '
                .'FROM transaction AS t '
                .'WHERE t.type = "'.Transaction::TYPE_DEPOSIT.'" '
                .'AND t.user_id = '.$user->getId().' '
                .'AND t.whitdrawal = 1 '
                .'ORDER BY t.created_at DESC LIMIT 1';
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $rtd_transaction = $stmt->fetchAll();


        if(count($rt_transaction) > 0){
                if( (new DateTime($rt_transaction[0]["created_at"])) > ($now)->modify('-10 minutes') ){
                        throw new ApiBadRequestException(["transaction" => "Para ejecutar otro retiro debe esperar 10 minutos desde tu ultimo retiro"]);
                }
        }

        if(count($rtd_transaction) > 0)
              if( (new DateTime($rtd_transaction[0]["created_at"])) > ($now)->modify('-10 minutes') ){
                      throw new ApiBadRequestException(["transaction" => "Para ejecutar otro retiro debe esperar 10 minutos desde tu ultimo retiro"]);
              }


        //throw new ApiBadRequestException(["transaction" => "stop: "./*(new DateTime($rtd_transaction[0]["created_at"]))->format('Y-m-d H:i:s')*/
        ///*( ((new DateTime($rtd_transaction[0]["created_at"])) > ($now)->modify('-10 minutes'))? "verdadero" : "falso")*/
        //count($rtd_transaction)
        //]);*/


        if ($user->getTwoAF()) {
            if (!isset($content["code"])) throw new ApiBadRequestException(['code' => 'Código 2FA requerido']);

            if (!$g->checkCode($user->getHash2FA(), $content["code"])) throw new ApiBadRequestException(['code' => 'Còdigo no vàlido']);
        }
        
        if ($content["amount"] < $dataBaseApp->getMinAmountWhitdrawal()) {
            throw new ApiBadRequestException(["amount" => "El monto mìnimo para retirar es ". $dataBaseApp->getMinAmountWhitdrawal()]);            
        }

        if ($context == 'balance') {
            if ($user->getBalance() < $content["amount"]) {
                throw new ApiBadRequestException(["amount" => "No tiene fondos suficientes."]);            
            }
            $amount = $content["amount"];
        }elseif ($context == 'deposit') {
            $transaction = $this
                ->em
                ->createQueryBuilder()
                ->select('t')
                ->from(Transaction::class, 't')
                ->where('t.type = :depositType')
                ->andWhere("t.id = :id")
                ->andWhere("t.user = :user")
                ->setParameters([
                    "depositType" => Transaction::TYPE_DEPOSIT,
                    "id" => $content["idTransaction"],
                    "user" => $user
                ])
                ->getQuery()
                ->getOneOrNullResult();
            if (!$transaction) {
                throw new ApiBadRequestException(["transaction" => "Transacciòn no encontrada."]);            
            }
            if ($transaction->getWhitdrawal()) {
                throw new ApiBadRequestException(["transaction" => "Transacciòn ya fue retirada."]);            
            }
            $dateTransaction = clone $transaction->getDate();
            $dateTransaction->add(new DateInterval("P{$dataBaseApp->getDaysDelayWhitdrawal()}D"));
            if ($dateTransaction > $now) {
                throw new ApiBadRequestException(["transaction" => "Aun no puede retirar esta inversión, la puede retirar a partir la fecha: {$dateTransaction->format('Y-m-d')}"]);            
            }
            $amount = $transaction->getAmount();
        }
        #$amount = 5;
        $commandString = "/usr/bin/python3 {$_SERVER['DIR_FILE_BOT']}/retiros.py {$user->getEmail()} {$content['wallet']} {$amount}";
        $salida = json_decode(shell_exec($commandString),true);
        $status = $salida["response"]["status"] == "success" ? "success" : "failed";
        #$status = "failed";
        #return $this->json(['cm'=> $commandString, 'd'=> $salida]);

        $whitdrawal = (new Transaction())
            ->setUser($user)
            ->setChargeUUID('')
            ->setTransaction(NULL)
            ->setAmount($amount)
            ->setDate($now)
            ->setType(Transaction::TYPE_WITHDRAWAL)
            ->setStatus($status)
            ->setBtc($salida['btc_conversion'])
            ->setMessage($salida);

        if ($context == "deposit") {
            if ($status == "success" ) {
                $transaction->setWhitdrawal(true);           
                $whitdrawal->setWhitdrawal(true);
            }
            $whitdrawal->setTransaction($transaction);
            $user->setDeposited();

        }
        if ($status == "success" && $context == "balance") {
            $user->setBalance();
        }
        $this->em->persist($whitdrawal);
        $this->em->flush();
        
        //$user->setDeposited();
        //$user->setBalance();

        $Balance = $this->em->getRepository(User::class)->getBalance($user->getId());
        $user->setBalance($Balance);

        $Deposited = $this->em->getRepository(User::class)->getDeposited($user->getId());
        $user->setDeposited($Deposited);

        $this->em->persist($user);
        $this->em->flush();
        
        $transactionSerialized = $serializerInterface->normalize($whitdrawal, null, ["groups" => ["transaction"]]);
        
        if ($status == "success"){
            $hash = md5(uniqid(rand(), true));
            $user->setHashLocked($hash);
            $this->em->persist($user);
            $this->em->flush();

            // Envio de Email de retiro
            $mailer->send('Retiro de saldo', 'email/withdrawal.html.twig', [
                'user' => $user, 
                'whitdrawal' => $whitdrawal,
                'wallet' => $content['wallet'],
                'hash' => $hash
            ], 
                $user->getEmail());
        }
    return $this->json([
        'transaction' => $transactionSerialized
    ]);
    }

    /**
     * @Route("/api/whitdrawal-availables", name="_availables", methods="GET")
     */
    public function whitdrawalAvailables(Request $request, SerializerInterface $serializerInterface)
    {
        $user = $this->getUser();

        /* ************************************ */
        /* ************************************ */

        $transactions = $this
            ->em
            ->createQueryBuilder()
            ->select('t')
            ->from(Transaction::class, 't')
            ->where("t.type = :typeDeposit")
            ->andWhere("t.status = :statusSuccess")
            ->andWhere("t.whitdrawal is NULL")
            ->andWhere("t.user = :user")
            ->setParameters([
                "typeDeposit" => Transaction::TYPE_DEPOSIT,
                "user" => $user,
                "statusSuccess" => "success"
            ])
            ->getQuery()
            ->getResult();

        $dataAppBase = $this->getDoctrine()->getRepository(PerformanceData::class)->findOneBy([]);
        $daysDelayWhitdrawal = $dataAppBase->getDaysDelayWhitdrawal();
        $now = new DateTime();
        $transactions = \array_filter($transactions, function ($transaction) use ($daysDelayWhitdrawal)
        {
            $now = new DateTime("now");
            $dias_transcurridos = $now->diff($transaction->getDate());

            if($dias_transcurridos->days >= $daysDelayWhitdrawal){
                return true;
            }
        }); 
        $transactions = \array_values($transactions);
        $transactionsSerialized = $serializerInterface->normalize($transactions, null, ["groups" => ["transaction"]]);

        /* ************************************ */
        /* ************************************ */

        $transactionsAll = $this
            ->em
            ->createQueryBuilder()
            ->select('t')
            ->from(Transaction::class, 't')
            ->where("t.type = :typeDeposit")
            ->andWhere("t.status = :statusSuccess")
            ->andWhere("t.user = :user")
            ->setParameters([
                "typeDeposit" => Transaction::TYPE_DEPOSIT,
                "user" => $user,
                "statusSuccess" => "success"
            ])
            ->getQuery()
            ->getResult();

        foreach ($transactionsAll as $trans) {

            $now = new DateTime("now");
            $dias_transcurridos = $now->diff($trans->getDate());
            $dias_faltantes = $daysDelayWhitdrawal - $dias_transcurridos->days;

            if($dias_faltantes > 0){
                $trans->setStatus($dias_faltantes);
            }else {
                $trans->setStatus('availables');
            }

            if ($trans->getWhitdrawal()) {
                $trans->setStatus('retired');
            }
            if ($trans->getLocked()) {
                $trans->setStatus('locked');
            }
        }

        return $this->json([
            "transactions" => $transactionsSerialized,
            "transactionsAll" => $transactionsAll,
        ]);
    } 
}
