<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminUserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = (new User())
            ->setEmail('karakorumtech@gmail.com')
            ->setName('Admin')
            ->setRoles(['ROLE_ADMIN']);
        $admin
            ->setPassword($this->passwordEncoder->encodePassword(
                            $admin,
                            'kara05122020'
                        ));

        $manager->persist($admin);

        $admin2 = (new User())
            ->setEmail('admin@gmail.com')
            ->setName('Admin')
            ->setRoles(['ROLE_ADMIN']);
        $admin2
            ->setPassword($this->passwordEncoder->encodePassword(
                            $admin2,
                            '12345'
                        ));

        $manager->persist($admin2);

        $master = (new User())
            ->setEmail('karakorumcorp@gmail.com')
            ->setName('Master');
        $master
            ->setPassword($this->passwordEncoder->encodePassword(
                            $master,
                            'master05122020'
                        ));

        $manager->persist($master);
        $manager->flush();
    }
}
