<?php

namespace App\Validator\Api\Access;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use App\Util\Validator;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ChanguePasswordValidator extends Validator
{
    public $security; 
    public $userPasswordEncoder;

    public function __construct(Security $security, UserPasswordEncoderInterface $userPasswordEncoder) {
        $this->security = $security;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }


    public function validate($request)
    {
        $requiredMessage = 'Este campo es requerido.';
        $constraint = [new Assert\Collection([
            'fields' => ['oldPassword' => [new Assert\Callback(['callback'=>[self::class, 'validateOldPassword'],'payload'=>['security' => $this->security, 'encoder'=> $this->userPasswordEncoder]]), new Assert\NotBlank()],
            'newPassword' => [new Assert\NotBlank(),],],
            'missingFieldsMessage' => 'El campo es requerido.',
        ])];
        
        parent::validateRequest($request, $constraint);
    }

    public  function validateOldPassword($object, ExecutionContextInterface $context, $payload)
    {
        if (!$payload['encoder']->isPasswordValid($payload['security']->getUser(), $object)) {
            $context->buildViolation('Clave inválida.')
                ->addViolation();        
            }
    }
}
