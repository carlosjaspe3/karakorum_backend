<?php

namespace App\Util;

use Twig\Environment;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Mailer
{
    private $mailer;
    private $template;
    private $parameterBagInterface;

    public function __construct(\Swift_Mailer $mailer,  Environment $template, ParameterBagInterface $parameterBagInterface) {
        $this->mailer = $mailer;
        $this->template = $template;
        $this->parameterBagInterface = $parameterBagInterface;
    }
    
    public function send($title, $template, $data, $to, $from = null, $reply = null)
    {
        /* if ($from == null || $from == ''){
            $from = $this->parameterBagInterface->get('app.sender_email');
        } */
        $from = "noreply@karakorumcorp.com";

        $body = $this->template->render($template, $data);
        $message = (new \Swift_Message($title))
            ->setFrom([$from => 'KarakorumCorp'])
            ->setTo($to)
            ->setBody($body, 'text/html');
        /**/
        $message->setReplyTo($reply);
        /**/
        $this->mailer->send($message);        
    }
}
