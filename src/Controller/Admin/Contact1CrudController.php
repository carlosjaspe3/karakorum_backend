<?php

namespace App\Controller\Admin;

use App\Entity\Contact1;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class Contact1CrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Contact1::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Contacto')
            ->setEntityLabelInPlural('Contactos')
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable('new')
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel('Detalle');
            })

            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-pencil')->setLabel('Modificar');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setIcon('fa fa-trash')->setLabel('Eliminar');
            })
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name', 'Título');
        $email = TextField::new('email', 'Email');
        $status = TextField::new('status', 'Estado');
        $description = TextareaField::new('description', 'Descripción');
        $createdAt = DateTimeField::new('createdAt', 'Fecha de creación');
        $updatedAt = DateTimeField::new('updatedAt', 'Fecha de actualización');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$name, $email, $status, $description, $createdAt];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$name, $email, $status, $description, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $email, $status, $description, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $email, $status, $description, $createdAt, $updatedAt];
        }
    }
}
