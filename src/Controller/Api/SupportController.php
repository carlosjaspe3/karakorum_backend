<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Util\Mailer;
use App\Entity\Contact;
use App\Entity\Contact1;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class SupportController extends AbstractController
{
    /**
     * @Route("/api/support", name="api-support")
     */
    public function supportAction(Request $request, Mailer $mailer)
    {
        $input = $request->getContent() ? json_decode($request->getContent(), true) : [];
        $em = $this->getDoctrine()->getManager();

        $support = new Contact();
        $support->setEmail($input['email']);
        $support->setMessage($input['message']);
        $support->setSubject($input['subject']);
        $support->setType($input['type']);
        $support->setStatus('new');
        
        $em->persist($support);
        $em->flush();

        /* ++++++++++++++++ */
        $mailer->send(
            'Soporte', 
            'email/support.html.twig', 
            ['support' => $support], 
            'karakoruminfo@gmail.com',
            '',
            $input['email']
        );
        /* ++++++++++++++++ */

        return new JsonResponse([
                'support' => $support,
        ]);        
    }


    /**
     * @Route("/api/contacto", name="api-contact")
     */
    public function contactAction(Request $request, Mailer $mailer)
    {
        $input = $request->getContent() ? json_decode($request->getContent(), true) : [];
        $em = $this->getDoctrine()->getManager();

        $contact = new Contact1();
        $contact->setName($input['name']);
        $contact->setEmail($input['email']);
        $contact->setDescription($input['description']);
        $contact->setStatus('new');
        
        $em->persist($contact);
        $em->flush();

        /* ++++++++++++++++ */
        $mailer->send(
            'Contacto', 
            'email/contact.html.twig', 
            ['contact' => $contact], 
            'karakoruminfo@gmail.com',
            '',
            $input['email']
        );
        /* ++++++++++++++++ */

        return new JsonResponse([
                'contact' => $contact,
        ]);        
    }

    ############################
    ########  TEST  ############
    ############################

    /**
     * @Route("/payment/resp/test", name="test", methods={"POST"})
     */
    public function test(Request $request): Response
    {
        return new Response('confirmation');
    }

    /**
     * @Route("/payment/resp/final", name="final")
     */
    public function final(Request $request): Response
    {
        return new Response('final');
    }

    /**
     * @Route("/email", name="email")
     */
    public function testemail(Request $request): Response
    {
        return $this->render(
            'access/base_email.html.twig');
    }
}
