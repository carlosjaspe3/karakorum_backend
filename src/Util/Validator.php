<?php

namespace App\Util;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use App\Exception\ApiBadRequestException;

class Validator
{
    private $violations;

    public function validateRequest($request, $constraint)
    {
        $validator = Validation::createValidator();
        $this->violations = $validator->validate($request, $constraint);
        if (count($this->violations) > 0) {
            throw new ApiBadRequestException($this->getErrors());
        }
    }

    public function getErrors()
    {
        $msj = [];
        foreach ($this->violations as $error) {
            $msj[str_replace(['[', ']'],'',$error->getPropertyPath())] = $error->getMessage();
        }
        return $msj;
    }

}
