<?php

namespace App\Repository;

use App\Entity\BasePerformance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BasePerformance|null find($id, $lockMode = null, $lockVersion = null)
 * @method BasePerformance|null findOneBy(array $criteria, array $orderBy = null)
 * @method BasePerformance[]    findAll()
 * @method BasePerformance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasePerformanceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BasePerformance::class);
    }

    // /**
    //  * @return BasePerformance[] Returns an array of BasePerformance objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BasePerformance
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
