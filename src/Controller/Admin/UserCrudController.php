<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\Referral;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Filter\BooleanFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\DateTimeFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\TextFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\NumericFilter;

class UserCrudController extends AbstractCrudController
{
    private $adminUrlGenerator;
    private $userPasswordEncoderInterface;
    private $entityManager;

    public function __construct(AdminUrlGenerator $adminUrlGenerator, UserPasswordEncoderInterface $userPasswordEncoderInterface, EntityManagerInterface $entityManager)
    {
        $this->adminUrlGenerator = $adminUrlGenerator;
        $this->userPasswordEncoderInterface = $userPasswordEncoderInterface;
        $this->entityManager = $entityManager;
    }

    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Usuario')
            ->setEntityLabelInPlural('Usuarios')
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        $sendConfirEmail = Action::new('sendConfirEmail', 'Reenviar email', 'fas fa-envelope')
            ->linkToRoute('confirmation_email_send', function (User $user): array {
                return [
                    'user' => $user->getId(),
                ];
            })
            ->displayIf(static function ($entity) {
                return $entity->getConfirmationToken();
            });

        $userDeposit = Action::new('userDeposit', 'Depositar', 'fa fa-credit-card')
            ->linkToCrudAction('showModalUserDeposit');

        $reset2fa = Action::new('reset2fa', 'Resetear 2FA', 'fab fa-keycdn')
            ->linkToCrudAction('reset2fa')
            ->displayIf(static function ($entity) {
                return $entity->getHash2FA();
            });

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel('Detalle');
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-pencil')->setLabel('Modificar');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setIcon('fa fa-trash')->setLabel('Eliminar');
            })
            ->add(Crud::PAGE_INDEX, $userDeposit)
            ->add(Crud::PAGE_INDEX, $sendConfirEmail)
            ->add(Crud::PAGE_INDEX, $reset2fa)
            ->reorder(Crud::PAGE_INDEX, [Action::DETAIL, 'userDeposit'])
        ;
    }

    public function showModalUserDeposit(AdminContext $context)
    {
        $user = $context->getEntity()->getInstance();
        $url = $this->adminUrlGenerator->setRoute('user_deposit_new')->generateUrl();
        
        return $this->render('admin/actions/deposit-modal.html.twig', [
            'user' => $user,
            'url' => $url,
        ]);
    }

    public function configureFields(string $pageName): iterable
    {
        $email = TextField::new('email', 'Email');
        $name = TextField::new('name', 'Nombre');
        $surname = TextField::new('surname', 'Apellido');
        $phone = TextField::new('phone', 'Télefono');
        $address = TextField::new('address', 'Dirección');
        $enabled = Field::new('enabled', 'Activo');
        $compoundInterest = Field::new('compoundInterest', 'Interes Compuesto');
        $plainPassword = Field::new('plainPassword', 'Contraseña');
        $hash2FA = TextareaField::new('hash2FA', 'Código para activar 2FA');
        $twoAF = Field::new('twoAF', '2FA Activo');
        $access2Fa = Field::new('access2Fa', 'Pedir 2FA para Acceder');
        $balance = NumberField::new('balance', 'Balance');
        $deposited = NumberField::new('deposited', 'Depositado');
        $createdAt = DateTimeField::new('createdAt', 'Fecha de registro');
        $updatedAt = DateTimeField::new('updatedAt', 'Fecha de actualización');
        $parent  = Field::new('parentfirstlevel', 'Padre')->formatValue(function ($value, $entity){
            //dd($value->getId());
            // $this->adminUrlGenerator
            // ->build()
            // ->setController(UserCrudController::class)
            // ->setAction(Action::DETAILS)
            // ->generateUrl();
            if($value){
                $url = $this->adminUrlGenerator->setEntityId($value->getId())->generateUrl();
                return '<a href="'.$url.'" target="_blank">'.$value->getEmail().'</a>';
            }
            return "-";
            
        });
        $id = IntegerField::new('id', 'ID');
        $referralCode  = Field::new('referralCode', 'Código de referidos');
        $referralCodeParent  = Field::new('walletBtcBlockio', 'Código de referido (Padre)');
        $confirmationToken = TextField::new('confirmationToken', 'Código Email Verificado')
            ->setTextAlign('center')
            ->setTemplatePath('admin/fields/led.html.twig');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $email, $name, $surname, $referralCode, $balance, $deposited, $enabled, $confirmationToken, $createdAt];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $email, $referralCode, $parent, $name, $surname, $phone, $address, $enabled, $compoundInterest, $confirmationToken, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$email, $name, $surname, $phone, $address, $referralCodeParent, $enabled, $plainPassword];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$email, $name, $surname, $phone, $address, $enabled, $compoundInterest , $plainPassword, $confirmationToken, $hash2FA, $twoAF, $access2Fa];
        }
    }

    public function persistEntity(EntityManagerInterface $entityManager, $user): void
    {
        $user->setPassword($this->userPasswordEncoderInterface->encodePassword($user, $user->getPlainPassword()));
        $user->setReferralCode(\uniqid());
        $entityManager->persist($user);

        $referralCodeParent = $user->getWalletBtcBlockio();
        $user->setWalletBtcBlockio(null);

        /* Relacion del padre */
        if($referralCodeParent){
            $parentUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(['referralCode'=>$referralCodeParent]);
            if(!$parentUser){
                throw new ApiBadRequestException("Código de referido invàlido");
            }
        } else {
            $parentUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=>2]);
        }

        foreach($parentUser->getParents() as $parent){
            $level = $this->getDoctrine()->getRepository(Referral::class)->findOneBy(['parent' => $parent->getParent(),'children'=>$parentUser])->getLevel();
            $referral = (new Referral())
                ->setChildren($user)
                ->setParent($parent->getParent())
                ->setLevel($level+1);
            $entityManager->persist($referral);
        }
        $referral = (new Referral())
            ->setChildren($user)
            ->setParent($parentUser)
            ->setLevel(1);
        $entityManager->persist($referral);
        /* Fin relacion del padre */

        $entityManager->flush();
    }

    public function updateEntity(EntityManagerInterface $entityManager, $user): void
    {
        if ($user->getPlainPassword()) {
            $user->setPassword($this->userPasswordEncoderInterface->encodePassword($user, $user->getPlainPassword()));
        }
        $entityManager->flush();
    }

    public function deleteEntity(EntityManagerInterface $entityManager, $user): void
    {
        $now = new \DateTime();
        $user->setEmail($user->getEmail().'-'.$now->format('d/m/Y-h:i:s'));
        $user->setIsDeleted(true);
        $user->setEnabled(false);
        $entityManager->flush();
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $response = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $response->andWhere('entity.isDeleted IS NULL')->andWhere('entity.roles NOT LIKE :roleAdmin')
                                                    ->setParameter('roleAdmin','%ROLE_ADMIN%');

        return $response;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(DateTimeFilter::new('createdAt', 'Fecha de registro'))
            ->add(TextFilter::new('name', 'Nombre'))
            ->add(TextFilter::new('referralCode', 'Link de referido'))
            ->add(TextFilter::new('email', 'Email'))
            ->add(BooleanFilter::new('enabled', 'Activo'))
            ->add(NumericFilter::new('balance', 'Balance'))
            ->add(NumericFilter::new('deposited', 'Depósito'))
        ;
    }

    public function reset2fa(AdminContext $context)
    {
        $user = $context->getEntity()->getInstance();

        $user->setHash2FA(NULL);
        $user->setAccess2Fa(false);
        $user->setTwoAF(false);
        
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $url = $this->adminUrlGenerator
            ->setController(UserCrudController::class)
            ->setAction(Action::INDEX)
            ->generateUrl();

        $this->addFlash('success', '2FA Reseteado con exito!');
        return $this->redirect($url);
    }
}
