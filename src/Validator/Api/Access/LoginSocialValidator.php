<?php

namespace App\Validator\Api\Access;

use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use App\Util\Validator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Exception\ApiBadRequestException;
use App\Exception\ApiBadRequestInfoException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoginSocialValidator extends Validator
{
    protected $em;

    public function validate($input) 
    {
        $constraint = new Assert\Collection([
            'fields' => [
                // 'email' => [new Assert\NotBlank(), new Assert\Email(['message' => 'Debe contener una dirección de email válida.'])],
                // 'surname' => [new Assert\NotBlank()],
                // 'name' => [new Assert\NotBlank()],
                'token' => [new Assert\NotBlank()],
                // 'fcmToken' => [],
            ],
            'missingFieldsMessage' => 'El campo es requerido.',
            "allowExtraFields" => true
        ]);
        
        parent::validateRequest($input, $constraint);
    }
}
