<?php

namespace App\Validator\Api\Access;

use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use App\Util\Validator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\ORM\EntityManagerInterface;

class RegisterValidator extends Validator
{

    protected $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function validate($input)
    {
        $constraint = new Assert\Collection([
            'fields' => ['email' => [new Assert\NotBlank(), new Assert\Email(['message' => 'Debe contener una dirección de email válida.']), new Assert\Callback(['callback'=>[self::class, 'validateEmail'],'payload'=>$this->em])],
            'name' => [new Assert\NotBlank(), new Assert\Type('string')],
            'password' => [new Assert\NotBlank()],
            'passwordRepeat' => [new Assert\NotBlank(), new Assert\Callback(['callback'=>[self::class, 'validatePassword'],'payload'=>$input])]
        ],
            'missingFieldsMessage' => 'El campo es requerido.',
            'allowExtraFields' => true
        ]);

        parent::validateRequest($input, $constraint);
    }
    public  function validatePassword($object, ExecutionContextInterface $context, $payload)
    {
        if (!isset($payload['password']) || !isset($payload['passwordRepeat'])) return;

        if ($payload['password'] != $payload['passwordRepeat']) {
            $context->buildViolation('Las contrseñas no son iguales')
                ->atPath('')
                ->addViolation();  
        }
    }

    public  function validateEmail($object, ExecutionContextInterface $context, $payload)
    {
        if (!$object) return;
        $user = $payload->getRepository(User::class)->findOneByEmail($object);
        if ($user){
            $context->buildViolation('Email no disponible.')
            ->addViolation();
        }
    }
}
