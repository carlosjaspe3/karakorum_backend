<?php

namespace App\Controller\Admin;

use App\Entity\PerformanceDataUser;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class PerformanceDataUserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PerformanceDataUser::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Configuración Rendimiento Usuario')
            ->setEntityLabelInPlural('Configuración Rendimiento Usuarios')
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel('Detalle');
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-pencil')->setLabel('Modificar');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setIcon('fa fa-trash')->setLabel('Eliminar');
            })
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $minAmount = NumberField::new('minAmount', 'Monto Min');
        $maxAmount = NumberField::new('maxAmount', 'Monto Max');
        $percentage = NumberField::new('percentage', 'Porcentaje (%)');
        $performanceData = AssociationField::new('performanceData', 'Datos de rendimiento');
        $id = IntegerField::new('id', 'ID');
        $createdAt = DateTimeField::new('createdAt', 'Fecha de creación');
        $updatedAt = DateTimeField::new('updatedAt', 'Fecha de actualización');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$minAmount, $maxAmount, $percentage];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $minAmount, $maxAmount, $percentage, $performanceData, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$minAmount, $maxAmount, $percentage, $performanceData];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$minAmount, $maxAmount, $percentage];
        }
    }
}
